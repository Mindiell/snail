# encoding: utf-8

from ordered_set import OrderedSet


class TestOrderedSet:
    def setup_method(self):
        self.simple_string = "This is a simple sentence."
        self.peer_string = "foo bar baz foo baz bar bar bar bas"
        self.simple_os = OrderedSet(self.simple_string)
        self.peer_os = OrderedSet(self.peer_string)

    def test_simple_string(self):
        assert len(self.simple_os.elements) == 5
        assert self.simple_os.elements[0] == "This"
        assert self.simple_os.elements[-1] == "sentence."

    def test_peer_string(self):
        assert len(self.peer_os.elements) == 4
        assert self.peer_os.elements[0] == "foo"
        assert self.peer_os.elements[-1] == "bas"

    def test_serializer(self):
        assert self.simple_os.serialize() == self.simple_string
