# encoding: utf-8
"""
Testing by creating a simple Tree :
        Tree
       0root0
        /  \
      1A0  4C1
      / \
    2B0 3D1
"""

from tree import Node, Tree


class TestTree:
    def setup_method(self):
        self.tree = Tree()
        self.node_A = Node()
        self.node_B = Node()
        self.node_C = Node()
        self.node_D = Node()
        # Building tree
        self.tree.root.add_child(self.node_A)
        self.node_A.add_child(self.node_B)
        self.tree.root.add_child(self.node_C)
        self.node_A.add_child(self.node_D)

    def test_root(self):
        assert self.tree.root.parent is None
        assert self.node_A.root is self.tree.root
        assert self.node_B.root is self.tree.root
        assert self.node_C.root is self.tree.root
        assert self.node_D.root is self.tree.root

    def test_parent(self):
        assert self.tree.root.parent is None
        assert self.node_A.parent is self.tree.root
        assert self.node_B.parent is self.node_A
        assert self.node_C.parent is self.tree.root
        assert self.node_D.parent is self.node_A

    def test_descendant(self):
        assert self.node_B.is_descendant(self.node_A)
        assert not self.node_B.is_descendant(self.node_C)
        assert self.node_B.is_inclusive_descendant(self.node_A)
        assert not self.node_A.is_descendant(self.node_A)
        assert self.node_A.is_inclusive_descendant(self.node_A)

    def test_ancestor(self):
        assert self.node_A.is_ancestor(self.node_B)
        assert not self.node_C.is_ancestor(self.node_B)
        assert self.node_A.is_inclusive_ancestor(self.node_B)
        assert not self.node_B.is_ancestor(self.node_B)
        assert self.node_B.is_inclusive_ancestor(self.node_B)

    def test_first_child(self):
        assert self.node_A.first_child is self.node_B
        assert self.node_A.first_child is not self.node_C
        assert self.node_C.first_child is None

    def test_last_child(self):
        assert self.node_A.last_child is self.node_D
        assert self.node_C.last_child is None

    def test_sibling(self):
        assert self.node_A.is_sibling(self.node_B) is False
        assert self.node_A.is_sibling(self.node_C) is True
        assert self.node_B.is_sibling(self.node_D) is True
        assert self.node_A.is_inclusive_sibling(self.node_C) is True
        assert self.node_A.is_sibling(self.node_A) is True
        assert self.node_A.is_inclusive_sibling(self.node_A) is True
        assert self.node_A.previous_sibling is None
        assert self.node_C.previous_sibling is self.node_A
        assert self.node_B.previous_sibling is None
        assert self.node_D.previous_sibling is self.node_B
        assert self.node_A.next_sibling is self.node_C
        assert self.node_C.next_sibling is None
        assert self.node_B.next_sibling is self.node_D
        assert self.node_D.next_sibling is None

    def test_index(self):
        assert self.node_A.index == 0
        assert self.node_B.index == 0
        assert self.node_C.index == 1
        assert self.node_D.index == 1

    def test_order(self):
        assert self.node_A.order == 1
        assert self.node_B.order == 2
        assert self.node_C.order == 4
        assert self.node_D.order == 3
