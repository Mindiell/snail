# encoding: utf-8
"""
https://dom.spec.whatwg.org/

Other URL:
- https://fr.wikipedia.org/wiki/Document_Object_Model
"""


def unique(sequence):
    seen = set()
    return [x for x in sequence if not (x in seen or seen.add(x))]


class OrderedSet:
    def __init__(self, string=""):
        self.elements = unique(string.split())

    def serialize(self):
        return " ".join(self.elements)
